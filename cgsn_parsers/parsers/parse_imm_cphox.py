#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@package cgsn_parsers.parsers.parse_imm_cphox
@file cgsn_parsers/parsers/parse_imm_cphox.py
@author Paul Whelan (copied from parse_testph.py)
@brief Parses the IMM output from the Deep SeapHOx V2 sensor.


<<<<< obsolete, use parse_cphox.py >>>>>>>>> - ppw 10/07/2024
<<<<< obsolete, use parse_cphox.py >>>>>>>>> - ppw 10/07/2024
<<<<< obsolete, use parse_cphox.py >>>>>>>>> - ppw 10/07/2024

"""
